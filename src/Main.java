import javax.swing.*;

public class Main {
    public static void main(String[] args)
    {
        Interfata interfata = new Interfata();
        interfata.setLayout(null);
        interfata.setSize(400,600);
        interfata.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        interfata.setVisible(true);
        interfata.setResizable(false);
    }
}
