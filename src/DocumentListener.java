import javax.print.Doc;
import javax.swing.event.DocumentEvent;
import java.util.EventListener;

/**
 * Created by Claudiu on 5/24/2016.
 */
public interface DocumentListener extends EventListener{
    public void insertUpdate(DocumentEvent e);
    public void removeUpdate(DocumentEvent e);
    public void changedUpdate(DocumentEvent e);
}
