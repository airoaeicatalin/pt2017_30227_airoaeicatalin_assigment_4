import javax.swing.*;
import javax.swing.event.*;
import javax.swing.event.DocumentListener;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.util.*;

public class Interfata extends JFrame{

    private Map<String,ArrayList<String>> dictionarMap;
    private Numbers s = new Numbers();
    private DictionarProxy dictionar1 = s.getDictionar1();

    private JPanel contentPanel;
    private JLabel Cuvant,Sinonim,Cuvant1;
    private JTextField CuvantText, SinonimText;
    private JTextField Cuvant1Text;
    private JTextField Search;
    private JButton add1,remove,X,S;

    private JTable table;
    private DefaultTableModel tableModel;
    private JScrollPane tableScroll;
    private JSeparator separator;

    public Interfata()
    {
        super("Tema 5");
        contentPanel = new JPanel();
        contentPanel.setBackground(Color.white);
        contentPanel.setLayout(null);

        Cuvant = new JLabel("Cuvant:");
        Cuvant.setBounds(10,30,50,20);
        CuvantText = new JTextField();
        CuvantText.setBounds(60,30,100,20);

        Cuvant1 = new JLabel("Cuvant:");
        Cuvant1.setBounds(10,90,50,20);

        Cuvant1Text = new JTextField();
        //Cuvant1Text.addItem("b");
        Cuvant1Text.setBounds(60,90,300,20);

        remove = new JButton("REMOVE:");
        remove.setBounds(60,120,300,20);
        X = new JButton("UPDATE");
        X.setBounds(20,165,350,20);
        S = new JButton("SEARCH");
        S.setBounds(160,5,100,20);

        Sinonim = new JLabel("Sinonime:");
        Sinonim.setBounds(190,30,70,20);
        SinonimText = new JTextField();
        SinonimText.setBounds(260,30,100,20);

        Search = new JTextField();
        Search.setBounds(20, 200,350,20);

        add1 = new JButton("ADD");
        add1.setBounds(60,60,300,20);

        String[] col = {"Cuvant","Sinonime"};
        Object[][] obj = {};

        tableModel = new DefaultTableModel(obj,col);
        table = new JTable(tableModel);
        tableScroll = new JScrollPane(table);
        tableScroll.setBounds(20,240,350,300);

        contentPanel.add(Cuvant);
        contentPanel.add(CuvantText);
        contentPanel.add(Sinonim);
        contentPanel.add(SinonimText);
        contentPanel.add(add1);
        contentPanel.add(Cuvant1);
        contentPanel.add(remove);
        contentPanel.add(Cuvant1Text);
        contentPanel.add(Search);
        contentPanel.add(tableScroll);
        contentPanel.add(X);
        contentPanel.add(S);

        Numbers n = new Numbers();
        add1.addActionListener(n);
        remove.addActionListener(n);
        X.addActionListener(n);
        S.addActionListener(n);


        this.setContentPane(contentPanel);
        this.setSize(800, 400);
        this.setResizable(false);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLocationRelativeTo(null);
        this.setVisible(true);
        this.addWindowListener(new WindowListener() {
            @Override
            public void windowOpened(WindowEvent e) {
                dictionar1 = SaveData.restoreDictionar();
                try
                {
                    dictionarMap = dictionar1.getDictionar();
                }
                catch (Exception ex)
                {

                }
                Iterator<String> iter = dictionarMap.keySet().iterator();
                while (iter.hasNext())
                {
                    String key = iter.next();
                    tableModel.addRow(new Object[]{key, dictionarMap.get(key).toString().replace("[", " ").toString().replace("]", " ")});
                }
            }

            @Override
            public void windowClosing(WindowEvent e) {
                SaveData.saveDictionar(dictionar1);
                System.exit(-1);
            }

            @Override
            public void windowClosed(WindowEvent e) {

            }

            @Override
            public void windowIconified(WindowEvent e) {

            }

            @Override
            public void windowDeiconified(WindowEvent e) {

            }

            @Override
            public void windowActivated(WindowEvent e) {

            }

            @Override
            public void windowDeactivated(WindowEvent e) {

            }
        });
    }

    private class Numbers implements ActionListener{
        private Criteria digit = new DigitCriteria();
        private Criteria special = new SpecialCriteria();
        private DictionarProxy dictionar = new DictionarProxy();

        public DictionarProxy getDictionar1()
        {
            return this.dictionar;
        }


        public String regex(String s) {
            System.out.println(s);
            String nou = s.replace("*", "\\w+");
            String nou2 = nou.replace("?", ".");
            return nou2;
        }
        public void actionPerformed(ActionEvent event)
        {
            JButton src = (JButton) event.getSource();

            if(src.equals(S))
            {
                String a = Search.getText();
                String x = regex(a);


                try
                {
                    dictionarMap = dictionar.getDictionar();
                }
                catch (Exception ex)
                {

                }
                for(String s: dictionarMap.keySet())
                {
                    for(String val: dictionarMap.get(s))
                    {
                        if(s.matches(x))
                        {
                            System.out.println(s+" = "+val+"\n");
                        }
                    }
                }
               /* Iterator<String> iter = dictionarMap.keySet().iterator();
                while(iter.hasNext())
                {
                    String key = iter.next();
                    if(key.equals(a)==true)
                        System.out.println(key + " ");
                }*/


            }


            if(src.equals(add1))
            {
                String a = CuvantText.getText().toString();
                String b = SinonimText.getText().toString();
                String[] sinonime = b.split(" ");
                ArrayList<String> sinonimeFinal = new ArrayList<String>();
                for(int i=0;i<sinonime.length;i++)
                {
                    if(dictionar.contineCuvant(sinonime[i]))
                        return;
                    sinonimeFinal.add(sinonime[i]);
                }
                if(digit.meetCriteria(a) && special.meetCriteria(b))
                {
                    dictionar.adaugaCuvant(a,sinonimeFinal);
                }
                else
                {
                    System.out.println("Nu indeplineste criteria");
                }
                //int x = dictionar.getNrOfCuvinte();
                CuvantText.setText("");
                SinonimText.setText("");
                //System.out.println(x+ " ");
            }
            if(src.equals(X))
            {
                tableModel.getDataVector().removeAllElements();
                try
                {
                    dictionarMap = dictionar.getDictionar();
                }
                catch (Exception ex)
                {

                }
                Iterator<String> iter = dictionarMap.keySet().iterator();
                while(iter.hasNext())
                {
                    String key = iter.next();
                    tableModel.addRow(new Object[]{key,dictionarMap.get(key).toString().replace("[", " ").toString().replace("]", " ")});
                }
            }

            if(src.equals(remove))
            {
                String a = Cuvant1Text.getText();
                dictionar.stergeCuvant(a);
                int x = dictionar.getNrOfCuvinte();
                System.out.println("remove");
                System.out.println(x+ " ");
            }
        }
    }
}
