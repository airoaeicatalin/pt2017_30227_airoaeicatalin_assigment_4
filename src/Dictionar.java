import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.jar.Pack200;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Dictionar implements DictionarInterface,Serializable{

    private Map<String, ArrayList<String>> dictionar;

    public Dictionar()
    {
        dictionar = new HashMap<String,ArrayList<String>>();
    }

    void setDictionar(Map<String,ArrayList<String>> dictionar)
    {
        this.dictionar = dictionar;
    }

    Map<String,ArrayList<String>> getDictionar()
    {
        return dictionar;
    }

    @Override
    public void adaugaCuvant(String cuvant, ArrayList<String> sinonime) {

        assert cuvant !=null && cuvant.length() > 0 && sinonime !=null &sinonime.size() >0;
        dictionar.put(cuvant,sinonime);

        for(String string : sinonime)
        {
            if(!contineCuvant(string))
            {
                ArrayList<String> listaSinonime = new ArrayList<String>();

                for(String s: sinonime)
                {
                    listaSinonime.add(s);
                }

                listaSinonime.remove(string);
                listaSinonime.add(cuvant);
                dictionar.put(string,listaSinonime);
            }
        }
        assert contineCuvant(cuvant);
    }

    @Override
    public void stergeCuvant(String cuvant) {

        assert cuvant != null && cuvant.length() > 0;
        if(getNrOfCuvinte() > 2)
        {
            ArrayList<String> sinonime = dictionar.get(cuvant); //copie de sinonime
            dictionar.remove(cuvant);

            for(String s: sinonime)
            {
                if(contineCuvant(s))
                {
                    dictionar.get(s).remove(cuvant);
                }
            }
        }
        else
        {
            dictionar.clear();
        }

        assert !contineCuvant(cuvant);
    }

    @Override
    public boolean contineCuvant(String cuvant) {

        for(Map.Entry<String, ArrayList<String>> entry : dictionar.entrySet())
        {
            if(cuvant.equals(entry.getKey().toString()))
            {
                return true;
            }
        }
        return false;
    }

    @Override
    public int getNrOfCuvinte() {
        int nr = 0;
        for(Map.Entry<String,ArrayList<String>> entry : dictionar.entrySet())
        {
            nr++;
        }
        return nr;
    }

    @Override
    public boolean isConsistent() {

        assert dictionar != null;
        for(Map.Entry<String, ArrayList<String>> entry : dictionar.entrySet())
        {
            for(String string : entry.getValue())
            {
                if(!contineCuvant(string))
                {
                    return false;
                }
            }
        }
        return true;
    }

    @Override
    public ArrayList<String> cauta(String pattern) {

        assert pattern != null;
        ArrayList<String> rezultat = new ArrayList<String>();
        if(pattern.length() > 0)
        {
            pattern = pattern.toLowerCase();
            pattern = pattern.replace("*", "[a-zA-Z]*");
            pattern = pattern.replace("?", "[a-zA-Z]");
            pattern = pattern + "[a-zA-Z]*";

            ArrayList<String> match = new ArrayList<String>();
            Pattern cautaP = Pattern.compile(pattern);
            Matcher patternMactcher;

            for(Map.Entry<String,ArrayList<String>> entry :dictionar.entrySet())
            {
                patternMactcher = cautaP.matcher(entry.getKey());
                if(patternMactcher.matches())
                {
                    match.add(entry.getKey());
                }
            }

            rezultat = match;
        }
        assert  rezultat != null;
        return rezultat;
    }
}
