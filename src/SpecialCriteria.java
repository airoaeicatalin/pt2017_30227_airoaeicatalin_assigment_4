
public class SpecialCriteria implements Criteria {
    @Override
    public boolean meetCriteria(String string) {
        for(int i=0;i<string.length();i++)
        {
            switch (string.charAt(i))
            {
                case '*': return false;
                case '&': return false;
                case '%': return false;
                case '^': return false;
            }
        }
        return true;
    }
}
