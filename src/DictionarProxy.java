import java.awt.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Map;

public class DictionarProxy implements DictionarInterface,Serializable{

    Dictionar dictionar = new Dictionar();

    public void adaugaCuvant(String cuvant, ArrayList<String> sinonime)
    {
        dictionar.adaugaCuvant(cuvant,sinonime);
    }

    public void stergeCuvant(String cuvant)
    {
        dictionar.stergeCuvant(cuvant);
    }

    public boolean contineCuvant(String cuvant)
    {
       return dictionar.contineCuvant(cuvant);
    }
    public boolean isConsistent()
    {
        return dictionar.isConsistent();
    }

    Map<String,ArrayList<String>> getDictionar()
    {
        return dictionar.getDictionar();
    }
    public int getNrOfCuvinte()
    {
        return dictionar.getNrOfCuvinte();
    }
    public ArrayList<String> cauta(String pattern)
    {
        return dictionar.cauta(pattern);
    }
}
