
public class DigitCriteria implements Criteria{
    @Override
    public boolean meetCriteria(String string) {
        for(char c : string.toCharArray())
        {
            if(Character.isDigit(c))
                return false;
        }
        return true;
    }
}
