import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class SaveData {

    public static void saveDictionar(DictionarProxy dictionar)
    {
        try
        {
            FileOutputStream file = new FileOutputStream("DictionarFile.save");
            ObjectOutputStream object = new ObjectOutputStream(file);
            object.writeObject(dictionar);
            object.close();
            file.close();
        }
        catch(Exception e)
        {
            System.out.println("Eroare salvare in fisier!" + e.toString());
        }
    }

    public static DictionarProxy restoreDictionar()
    {
        try
        {
            FileInputStream file = new FileInputStream("DictionarFile.save");
            ObjectInputStream object = new ObjectInputStream(file);
            DictionarProxy dictionar = (DictionarProxy) object.readObject();
            object.close();
            return dictionar;
        }
        catch (Exception e)
        {
            System.out.println("Eroare Incarcare Fisier!");
        }
        return null;
    }
}
