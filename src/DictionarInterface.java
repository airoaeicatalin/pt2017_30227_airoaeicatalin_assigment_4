import java.util.ArrayList;

public interface DictionarInterface {
    /**
     * Adauga un nou cuvant in dictionar
     * @param word cuvantul ce trebuie adaugat
     * @param sinonime lista de sinonime pentru cuvant
     */
    void adaugaCuvant(String cuvant, ArrayList<String> sinonime);

    /**
     * Sterge cuvant din dictionar
     * @param cuvant cuvantul sters
     */
    void stergeCuvant(String cuvant);

    /**
     * Verifica daca cuvantul se afla in dictionar
     * @param cuvant cuvantul cautat
     * @return true or false
     */
    boolean contineCuvant(String cuvant);

    /**
     * returneaza numarul de cuvinte din dictionar
     * @return numarul de cuvinte din dictionar
     */
    int getNrOfCuvinte();

    /**
     * Verifica daca dictionarul este consistent sau nu
     * @return true or false;
     */
    boolean isConsistent();

    /**
     * Cauta cuvantul in dictionar
     * @param pattern cuvantul cautat
     * @return returneaza lista de sinonime
     */
    ArrayList<String> cauta(String pattern);


}
